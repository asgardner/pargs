/*
Copyright (c) 2023, Arizona Board of Regents on behalf of
The University of Arizona. All rights reserved.

This program is free software: you can redistribute it and/or modify it under
the terms of the GNU General Public License as published by the Free Software
Foundation, either version 3 of the License, or (at your option) any later
version.

This program is distributed in the hope that it will be useful, but WITHOUT ANY
WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A
PARTICULAR PURPOSE. See the GNU General Public License for more details.

You should have received a copy of the GNU General Public License along with
this program. If not, see <https://www.gnu.org/licenses/>.
*/

#include <boost/format.hpp>
#include <boost/tokenizer.hpp>
#include <filesystem>
#include <fstream>
#include <iostream>
#include <list>

static void usage() {
  std::cout << "usage: pargs <PID>, where <PID> is the process ID (or IDs) to be "
               "queried in base 10."
            << std::endl;
}

static std::list<std::string> split(std::string text, boost::char_separator<char> sep) {
  std::list<std::string> list;
  boost::tokenizer<boost::char_separator<char>> tokens(text, sep);
  for (const auto& tok : tokens) {
    list.push_back(tok);
  }
  return list;
}

static void dezero(std::string& s) {
  for (auto it = s.begin(); it != s.end(); ++it) {
    if (*it == 0) {
      *it = ' ';
    }
  }
}

static std::string fileToString(std::string path) {
  auto ifstream = std::ifstream(path);
  std::stringstream buf;
  buf << ifstream.rdbuf();
  return buf.str();
}

int main(int argc, char* argv[]) {
  // Check argument number.
  if (argc < 2) {
    usage();
    return -1;
  }

  for (int i = 1; i < argc; i++) {
    // Is the argument a PID?
    pid_t pid = std::atoll(argv[i]);
    if (pid == 0) {
      usage();
      return -1;
    }

    // Check to see if the pid exists.
    auto path = std::filesystem::path(boost::str(boost::format("/proc/%llu") % pid));
    if (!std::filesystem::exists(path)) {
      std::cout << boost::format("Could not find '%s'") % path.generic_string() << std::endl;
      return -1;
    } else {
      std::cout << boost::format("[%d] PID %llu") % (i - 1) % pid << std::endl;
    }

    // Read in the command line.
    auto cmdline = fileToString(boost::str(boost::format("/proc/%llu/cmdline") % pid));
    dezero(cmdline);
    auto args = split(cmdline, boost::char_separator<char>(" "));

    // Write out the command line.
    auto it = args.begin();
    for (int i = 0; i < args.size(); i++, ++it) {
      std::cout << boost::format("%2d: %s") % i % *it << std::endl;
    }
  }

  return 0;
}
