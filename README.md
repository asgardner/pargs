# pargs

`pargs` as on Solaris, but for Linux. Given process IDs, print arguments in a human-readable format.

## License.

Copyright (c) 2023 The Arizona Board of Regents on behalf of The University of Arizona. All rights reserved.

![The University of Arizona logo](ua_horiz_rgb_4.png)

![GPL v3 logo](gplv3.png)

## Usage.

```bash
$ pgrep gjs | head -1 | xargs pargs
[0] PID 2197
 0: /usr/bin/gjs
 1: /usr/share/gnome-shell/org.gnome.Shell.Notifications
```

or 

```bash
$ pgrep chrome | tail -1 | xargs pargs
[0] PID 54553
 0: /opt/google/chrome/chrome
 1: --type=renderer
 2: --crashpad-handler-pid=3531
 3: --enable-crash-reporter=00000000-0000-0000-0000-000000000000,
 4: --change-stack-guard-on-fork=enable
 5: --lang=en-US
 6: --num-raster-threads=4
 7: --enable-main-frame-before-activation
 8: --renderer-client-id=542
 9: --time-ticks-at-unix-epoch=-1672851083967796
10: --launch-time-ticks=10522802323
11: --shared-files=v8_context_snapshot_data:100
12: --field-trial-handle=0,i,00000000000000000000,000000000000000000,000000
```

or 

```bash
$ pargs 2197 54553
[0] PID 2197
 0: /usr/bin/gjs
 1: /usr/share/gnome-shell/org.gnome.Shell.Notifications
[1] PID 54553
 0: /opt/google/chrome/chrome
 1: --type=renderer
 2: --crashpad-handler-pid=3531
 3: --enable-crash-reporter=00000000-0000-0000-0000-000000000000,
 4: --change-stack-guard-on-fork=enable
 5: --lang=en-US
 6: --num-raster-threads=4
 7: --enable-main-frame-before-activation
 8: --renderer-client-id=542
 9: --time-ticks-at-unix-epoch=-1672851083967796
10: --launch-time-ticks=10522802323
11: --shared-files=v8_context_snapshot_data:100
12: --field-trial-handle=0,i,00000000000000000000,000000000000000000,000000
```
